<?php

namespace Ciebit;

class TemplateInterface
{
    private $css_arquivos;
    private $css_codigo;
    private $html;
    private $js_arquivos;
    private $js_codigo;

    public function __construct(string $html)
    {
        $this->css_arquivos = [];
        $this->css_codigo = '';
        $this->html = $html;
        $this->js_arquivos = [];
        $this->js_codigo = '';
    }

    public function defCss(array $css):self
    {
        $this->css_arquivos = $css;
        return $this;
    }

    public function defCssCodigo(string $css): self
    {
        $this->css_codigo = $css;
        return $this;
    }

    public function defJs(array $js):self
    {
        $this->js_arquivos = $js;
        return $this;
    }

    public function defJsCodigo(string $js): self
    {
        $this->js_codigo = $js;
        return $this;
    }

    public function obterCss():array
    {
        return $this->css_arquivos;
    }

    public function obterCssCodigo(): string
    {
        return $this->css_codigo;
    }

    public function obterJs():array
    {
        return $this->js_arquivos;
    }

    public function obterJsCodigo(): string
    {
        return $this->js_codigo;
    }

    public function obterHtml():string
    {
        return $this->html;
    }
}
