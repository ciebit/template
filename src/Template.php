<?php
namespace Ciebit;

class Template
{
    public static function gerar( $arquivo, $_TEMPLATE = null )
    {
        if( !is_file($arquivo)) {
            throw new \Exception("O arquivo '{$arquivo}'' não foi encontrado");
        }

        $_TEMPLATE_CSS = [];
        $_TEMPLATE_CSS_CODIGO = '';
        $_TEMPLATE_JS = [];
        $_TEMPLATE_JS_CODIGO = '';

        ob_start();
        include $arquivo;
        $_TEMPLATE_HTML = ob_get_contents();
        ob_end_clean();

        $Pagina = new TemplateInterface($_TEMPLATE_HTML);
        $Pagina->defCss($_TEMPLATE_CSS);
        $Pagina->defCssCodigo($_TEMPLATE_CSS_CODIGO);
        $Pagina->defJs($_TEMPLATE_JS);
        $Pagina->defJsCodigo($_TEMPLATE_JS_CODIGO);

        return $Pagina;
    }
}
