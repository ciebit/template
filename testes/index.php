<?php
require __DIR__.'/../src/Template.php';
require __DIR__.'/../src/TemplateInterface.php';

$Template = \Ciebit\Template::gerar(
    'layout.php',
    ['titulo' => 'Título', 'paragrafo' => 'Paragráfo']
);

$css_lista = $Template->obterCss();
$js_lista = $Template->obterJs();
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title>Teste de Template</title>
        <?php foreach ($css_lista as $css) { ?>
            <link type="text/css" rel="stylesheet" href="<?= $css ?>">
        <?php } ?>
        <?php foreach ($js_lista as $js) { ?>
            <script type="text/javascript" src="<?= $js ?>"></script>
        <?php } ?>
    </head>
    <body>
        <?= $Template->obterHtml() ?>
    </body>
</html>
